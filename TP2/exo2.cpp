#include <QApplication>
#include <time.h>

#include "tp2.h"

MainWindow* w=nullptr;

void insertionSort(Array& toSort){
	Array& sorted=w->newArray(toSort.size());
	// insertion sort from toSort to sorted
    for (uint i=0; i < toSort.size(); i++) {
		if(i == 0) {
			sorted[0] = toSort[i];
		} else {
			bool alreadyInsert = false;
			for (uint t=0; t < toSort.size(); t++) {
				if (sorted[t] > toSort[i]) {
                    sorted.insert(t, toSort[i]);
					alreadyInsert = true;
					break;
				}
			}
			if (!alreadyInsert) {
				sorted.insert(i, toSort[i]);
			}
		}
        
    }
	
	toSort=sorted; // update the original array
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    uint elementCount=15; // number of elements to sort
    MainWindow::instruction_duration = 100; // delay between each array access (set, get, insert, ...)
    w = new TestMainWindow(insertionSort); // window which display the behavior of the sort algorithm
	w->show();

	return a.exec();
}
