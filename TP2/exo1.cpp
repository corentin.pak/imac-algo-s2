#include <QApplication>
#include <time.h>

#include "tp2.h"

MainWindow* w = nullptr;

void selectionSort(Array& toSort) {
    for (uint t = 0; t < toSort.size() - 1; t++) {
        uint minIndex = t;
        for (uint i = t + 1; i < toSort.size(); i++) {
            if (toSort[i] < toSort[minIndex]) {
                minIndex = i;
            }
        }
        if (minIndex != t) {
            toSort.swap(t, minIndex);
        }
    }
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
    uint elementCount=15; // number of elements to sort
    MainWindow::instruction_duration = 100; // delay between each array access (set, get, insert, ...)
    w = new TestMainWindow(selectionSort); // window which display the behavior of the sort algorithm
    w->show();

	return a.exec();
}
