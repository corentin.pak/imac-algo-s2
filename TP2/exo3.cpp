#include <QApplication>
#include <time.h>

#include "tp2.h"

MainWindow* w=nullptr;


void bubbleSort(Array& toSort){
	// bubbleSort
    for (uint i=0; i < toSort.size(); i++) {
        for (uint t=0; t < toSort.size()-1; t++) {
            if (toSort[t+1] < toSort[t]) {
                toSort.swap(t+1, t);
            }
        }
    }
}


int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	uint elementCount=20;
	MainWindow::instruction_duration = 100;
	w = new TestMainWindow(bubbleSort);
	w->show();

	return a.exec();
}
