#include <iostream>

using namespace std;

struct Noeud{
    int donnee;
    Noeud* suivant;
};

struct Liste{
    Noeud* premier;
    // your code
};

struct DynaTableau{
    int* donnees;
    int taille;
    int capacite;
    // your code
};


void initialise(Liste* liste)
{
    liste->premier = nullptr;
}

bool est_vide(const Liste* liste)
{
    if(liste->premier == nullptr) {
        return true;
    } else {
        return false;
    }
    
}

void ajoute(Liste* liste, int valeur)
{
    Noeud* newNoeud = new Noeud;
    newNoeud->donnee = valeur;
    newNoeud->suivant = liste->premier;
    liste->premier = newNoeud;
}

void affiche(const Liste* liste)
{
    Noeud* current;
    current = liste->premier;
    while (current != nullptr) {
        cout << current->donnee << endl;
        current = current->suivant;
    }
}

int recupere(const Liste* liste, int n)
{
    Noeud* current;
    current = liste->premier;
    for(int i = 0; i<n; i++) {
        current = current->suivant;
    }
    return current->donnee;
}

int cherche(const Liste* liste, int valeur)
{
    Noeud* current;
    current = liste->premier;
    int itterateur = 1;
    while (current != nullptr) {
        if (current->donnee == valeur) {
            return itterateur;
        }
        itterateur++;
        current = current->suivant;
    }
    return -1;
}

void stocke(Liste* liste, int n, int valeur)
{
    Noeud *current = liste->premier;
    for(int i = 0; i<n; i++) {
        current = current->suivant;
    }
    current->donnee = valeur;
}

void ajoute(DynaTableau* tableau, int valeur)
{
    if (tableau->capacite < tableau->taille + 1) { // Si la capacité du tableau ne suffit plus, on l'augmente
        tableau->capacite += 1;
        int *newTab = new int[tableau->capacite];
        for (int i = 0; i < tableau->taille; i++) {
            newTab[i] = tableau->donnees[i];
        }
        tableau->donnees = newTab;
    }
    tableau->donnees[tableau->taille] = valeur;
    tableau->taille += 1;
}


void initialise(DynaTableau* tableau, int capacite)
{
    tableau->capacite = capacite;
    tableau->taille = 0;
    tableau->donnees = new int [capacite];
}

bool est_vide(const DynaTableau* liste)
{
    if(liste->taille == 0) {
        return true;
    } else {
        return false;
    }
}

void affiche(const DynaTableau* tableau)
{
    for (int i = 0; i < tableau->taille; i++) {
        cout << "valeur " << i << " : " <<tableau->donnees[i] << endl;
    }
}

int recupere(const DynaTableau* tableau, int n)
{
    // On vérifié que n existe dans le tableau
    if (tableau->taille > n) {
        return tableau->donnees[n];
    }
    return 0;
}

int cherche(const DynaTableau* tableau, int valeur)
{
    for(int i = 0; i < tableau->taille; i++) {
        if (tableau->donnees[i] == valeur) {
            return i;
        }
    }
    return -1;
}

void stocke(DynaTableau* tableau, int n, int valeur)
{
    if (tableau->taille > n) {
        tableau->donnees[n] = valeur;
    }
}

//void pousse_file(DynaTableau* liste, int valeur)
void pousse_file(Liste* liste, int valeur)
{
    ajoute(liste, valeur);
}

//int retire_file(Liste* liste)
int retire_file(Liste* liste)
{
    if (liste->premier != nullptr) {
        int valeur = liste->premier->donnee;
        liste->premier = liste->premier->suivant;
        return valeur;
    }
    return 0;
    
}

//void pousse_pile(DynaTableau* liste, int valeur)
void pousse_pile(Liste* liste, int valeur)
{
    Noeud *current = liste->premier;
    while(current->suivant != nullptr) {
        current = current->suivant;
    }
    
    Noeud *newNoeud = new Noeud;
    newNoeud->donnee = valeur;
    current->suivant = newNoeud;
}

//int retire_pile(DynaTableau* liste)
int retire_pile(Liste* liste)
{
    Noeud *current = liste->premier;

    if (liste->premier->suivant == NULL) {
        int valeur = liste->premier->donnee;
        liste->premier = NULL;
        return valeur;
    }

    while (current->suivant != nullptr && current->suivant->suivant != nullptr) {
        current = current->suivant;
    }

    int valeur = current->suivant->donnee;
    current->suivant = nullptr;
    return valeur;
}


int main()
{
    Liste liste;
    initialise(&liste);
    DynaTableau tableau;
    initialise(&tableau, 5);
    if (!est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (!est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }
    for (int i=7; i>=1; i--) {
        ajoute(&liste, i*7);
        ajoute(&tableau, i*5);
    }

    if (est_vide(&liste))
    {
        std::cout << "Oups y a une anguille dans ma liste" << std::endl;
    }

    if (est_vide(&tableau))
    {
        std::cout << "Oups y a une anguille dans mon tableau" << std::endl;
    }
    std::cout << "Elements initiaux:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    std::cout << "5e valeur de la liste " << recupere(&liste, 4) << std::endl;
    std::cout << "5e valeur du tableau " << recupere(&tableau, 4) << std::endl;

    std::cout << "21 se trouve dans la liste à " << cherche(&liste, 21) << std::endl;
    std::cout << "15 se trouve dans la liste à " << cherche(&tableau, 15) << std::endl;

    stocke(&liste, 4, 7);
    stocke(&tableau, 4, 7);

    std::cout << "Elements après stockage de 7:" << std::endl;
    affiche(&liste);
    affiche(&tableau);
    std::cout << std::endl;

    Liste pile; // DynaTableau pile;
    Liste file; // DynaTableau file;

    initialise(&pile);
    initialise(&file);

    for (int i=1; i<=7; i++) {
        pousse_file(&file, i);
        pousse_pile(&pile, i);
    }

    int compteur = 10;
    while(!est_vide(&file) && compteur > 0)
    {
        std::cout << retire_file(&file) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    compteur = 10;
    while(!est_vide(&pile) && compteur > 0)
    {
        std::cout << retire_pile(&pile) << std::endl;
        compteur--;
    }

    if (compteur == 0)
    {
        std::cout << "Ah y a un soucis là..." << std::endl;
    }

    return 0;
}
