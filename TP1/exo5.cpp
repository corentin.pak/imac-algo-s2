#include "tp1.h"
#include <QApplication>
#include <time.h>

int isMandelbrot(Point z, int n, Point point){
    
    // recursiv Mandelbrot
    // check n
    if (n == 0) {
        return 1;
    }

    double x = z.x;
    double y = z.y;
    Point z_carre = Point(x * x - y * y, 2 * x * y);
    Point z_next;

    z_next.x = point.x + z_carre.x;
    z_next.y = point.y + z_carre.y;
    // check length of z
    double length = sqrt(z_next.x * z_next.x + z_next.y * z_next.y);
    if (length > 2) {
        return n;
    }
    // if Mandelbrot, return 1 or n (check the difference)
    // otherwise, process the square of z and recall
    // isMandebrot
    return isMandelbrot(z_next, n-1, point);
}

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow* w = new MandelbrotWindow(isMandelbrot);
    w->show();

    a.exec();
}



