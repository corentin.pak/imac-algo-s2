#include "mainwindow.h"
#include "tp3.h"
#include <QApplication>
#include <time.h>
#include <stack>
#include <queue>

MainWindow* w = nullptr;
using std::size_t;

struct SearchTreeNode : public Node
{    
    SearchTreeNode* left;
    SearchTreeNode* right;
    int value;

    void initNode(int value)
    {
        // init initial node without children
        this->value = value;
        this->left = nullptr;
        this->right = nullptr;
    }

	void insertNumber(int value) {
        // create a new node and insert it in right or left child
        SearchTreeNode* node = new SearchTreeNode(value);
        node->initNode(value);
        if(value < this->value) {
            this->left = node;
        } else if (value > this->value) {
            this->right = node;
        }
    }

	uint height() const {
        uint leftHeight = 0;
        uint rightHeight = 0;

        if (this->left != nullptr) {
            leftHeight = this->left->height();
        }

        if (this->right != nullptr) {
            rightHeight = this->right->height();
        }

        if (leftHeight > rightHeight) {
            return leftHeight + 1;
        } else {
            return rightHeight + 1;
        }
    }   

	uint nodesCount() const {
        int count = 1;

        if (this->left != nullptr) {
            count += this->left->nodesCount();
        }

        if (this->right != nullptr) {
            count += this->right->nodesCount();
        }
        return count;
	}

	bool isLeaf() const {
        if(this->left == nullptr && this->right == nullptr) {
            return true;
        }
        // return True if the node is a leaf (it has no children)
        return false;
	}

	void allLeaves(Node* leaves[], uint& leavesCount) {
        // fill leaves array with all leaves of this tree
        if (this->isLeaf()) {
            leaves[leavesCount++] = this;
        } else if (this->left != nullptr) {
            this->left->allLeaves(leaves, leavesCount);
        } else {
            this->left->allLeaves(leaves, leavesCount);
        }
	}

	void inorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with inorder travel
        if(this->left != nullptr) {
            this->left->inorderTravel(nodes, nodesCount);
        } 
        
        nodes[nodesCount] = this;
        nodesCount++;
        if (this->right != nullptr){
            this->right->inorderTravel(nodes, nodesCount);
        }
        
	}

	void preorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with preorder travel7
        nodes[nodesCount] = this;
        nodesCount++;

        if(this->left != nullptr) {
            this->left->preorderTravel(nodes, nodesCount);
        } 

        if (this->right != nullptr){
            this->right->preorderTravel(nodes, nodesCount);
        }
	}

	void postorderTravel(Node* nodes[], uint& nodesCount) {
        // fill nodes array with all nodes with postorder travel
        if(this->left != nullptr) {
            this->left->postorderTravel(nodes, nodesCount);
        } 

        if (this->right != nullptr){
            this->right->postorderTravel(nodes, nodesCount);
        }

        nodes[nodesCount] = this;
        nodesCount++;
	}

	Node* find(int value) {
        if (this->value == value) {
            return this;
        } else if (this->value > value) {
            this->left->find(value);
        } else if (this->value < value) {
            this->right->find(value);
        }
        // find the node containing value
		return nullptr;
	}

    void reset()
    {
        if (left != NULL) 
            delete left;
        if (right != NULL)
            delete right;
        left = right = NULL;
    }

    SearchTreeNode(int value) : Node(value) {initNode(value);}
    ~SearchTreeNode() {}
    int get_value() const {return value;}
    Node* get_left_child() const {return left;}
    Node* get_right_child() const {return right;}
};

Node* createNode(int value) {
    return new SearchTreeNode(value);
}

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow::instruction_duration = 200;
    w = new BinarySearchTreeWindow<SearchTreeNode>();
	w->show();

	return a.exec();
}
